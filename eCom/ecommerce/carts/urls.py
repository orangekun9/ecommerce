from django.conf.urls import url, include
from .views import (
        cart_update,
        cart_home
        )

urlpatterns = [
    url(r'^$', cart_home, name='home'),
    url(r'^update/$', cart_update, name='update')
]
