from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import contactForm, loginForm, registerForm
from django.contrib.auth import authenticate, login, get_user_model

def homePage(request):
    return render(request, 'home.html', {})

def contactPage(request):
    contact_form = contactForm()
    context = {
        'val': "This is the conteact us page of eCom",
        'form': contact_form
    }
    return render(request, 'views/contact.html', context)

def aboutPage(request):
    context = {
        'val': "This is the About Us page of eCom"
    }
    return render(request, 'views/about.html', context)

def loginPage(request):
    login_form = loginForm(request.POST or None)
    context = {
        "Form": login_form
    }
    print("User logged in")
    print(request.user.is_authenticated())
    if login_form.is_valid():
        print(login_form.cleaned_data) #Gives feild info(all data)
        username = login_form.cleaned_data.get("username")
        password = login_form.cleaned_data.get("password")
        user = authenticate(request, username=username, password=password)
        print(request.user.is_authenticated())
        if user is not None:
            print(request.user.is_authenticated())
            login(request, user)
            # Redirect to success page
            #context['Form'] = loginForm()
            return redirect('/login/')
        else:
            print("Error during login")
    return render(request, 'auth/login.html', context)

User = get_user_model()
def registerPage(request):
    register_form = registerForm(request.POST or None)
    if register_form.is_valid():
        print(register_form.cleaned_data)
        username = register_form.cleaned_data.get("username")
        email = register_form.cleaned_data.get("email")
        password = register_form.cleaned_data.get("password")
        new_user = User.objects.create_user(username, password, email)
        print(new_user)

    context = {
        'Form': register_form
    }
    return render(request, 'auth/register.html', context)