from django import forms
from django.contrib.auth import get_user_model
#from django.core.exceptions import ValidationError
User = get_user_model()


class contactForm(forms.Form):
    fullname = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control", "placeholder":"Your full name"}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={"class":"form-control", "placeholder": "Your email id"}))
    content = forms.CharField(widget=forms.Textarea(attrs={"class":"form-control", "placeholder": "Your Content"}))


class loginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Your Username"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control", "placeholder": "Your Password"}))


class registerForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control", "placeholder":"Your user name"}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={"class":"form-control", "placeholder":"Your email id"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control", "placeholder":"Your password"}))
    confirm = forms.CharField(label='Confirm Password',widget=forms.PasswordInput(attrs={"class":"form-control", "placeholder":"Re enter your password"}))
    #.....For labeling (Example).......
    #  confirm = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={"class":"form-control", "placeholder":"Re enter your password"}))

    def clean_username(self):
        username = self.cleaned_data.get("username")
        qs = User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError("Username is already taken")
        return username    
    
    def clean_email(self):
        email = self.cleaned_data.get("email")
        qs = User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("Email is already in use")
        return email
    
    def clean_confirm(self):
        data = self.cleaned_data
        password = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("confirm")
        
        if password != password2:
            #print("This ain't gona work")
            raise forms.ValidationError("Passwords must match.")
        return data    