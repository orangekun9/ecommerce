from django.shortcuts import render
from django.views.generic import ListView
from products.models import Product
# from django.db.models import Q

class SearchProductView(ListView):
    template_name = "search/view.html"

    def get_context_data(self, *args, **kwargs):
        context = super(SearchProductView, self).get_context_data(*args, **kwargs)
        query = self.request.GET.get('q')
        context['query'] = query
        return context

    def get_queryset(self, *args, **kwargs):  #get_queryset method is similar to get_object method
        request = self.request
        print(request.GET)
        query = request.GET.get('q')
        if query is not None :
            # lookups = Q(title__icontains=query) | Q(description__icontains=query)
            # return Product.objects.filter(title__icontains=query)
            # return Product.objects.filter(lookups).distinct()
            return Product.objects.search(query)
        return Product.objects.none()
